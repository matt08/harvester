﻿/*
 * TODO:
 * related
 * comment additional info and responses
 * move xpaths to resources
 * add source uri
 * error handling
*/

using Harvester.Core.Models;
using Harvester.Core.Parsers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Harvester.CommandLine
{
    class Program
    {
        private static void PrintTitles(ICollection<string> titles)
        {
            foreach (var detail in titles)
            {
                Console.WriteLine(String.Format("[*] {0}", detail));
            }
        }

        private static void PrintDetails(ICollection<Digg> diggs)
        {
            foreach(var digg in diggs)
            {
                Console.WriteLine(String.Format("[*] {0}", digg.Title));
                Console.WriteLine(String.Format("\t{0}", digg.Description));
                Console.WriteLine(String.Format("uri: {0}", digg.Url.AbsoluteUri));
            }
        }

        private static void PrintDiggDetail(DiggDetail digg)
        {
            Console.WriteLine(digg);
            Console.WriteLine("[*] Tags:");
            foreach (var tag in digg.Tags)
            {
                Console.Write(String.Format("{0}, ", tag));
            }
            Console.WriteLine();
            Console.WriteLine("[*] Comments:");
            foreach (var comment in digg.Comments)
            {
                Console.WriteLine(String.Format("[cmt: {0}]: {1}", comment.User, comment.Body));
            }
        }

        static void Main(string[] args)
        {
            var parser = new WykopParser();
            parser.LoadMainPage();
            var list = parser.GetDiggsList();

            foreach(var dig in list)
            {
                Console.WriteLine(dig.Title);
            }

            while (true)
            {
                Digg singleDigg = list.ToList<Digg>()[new Random().Next(0, list.Count - 1)];
                Uri diggUri = singleDigg.Url;
                DiggDetail digg = parser.LoadDigg(diggUri);
                PrintDiggDetail(digg);
                Console.ReadLine();
            }
        }
    }
}
