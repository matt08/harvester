﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Harvester.Core;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Harvester.Core.Utilities;
using Harvester.Core.Parsers;
using Harvester.Core.Models;


namespace Harvester.Tests
{
    [TestClass]
    public class ParserTests
    {
        StreamReader htmlStream;
        WykopParser parser;

        public ParserTests()
        {
            htmlStream = new StreamReader("TestFiles/main.htm", Encoding.UTF8);
            parser = new WykopParser(htmlStream);
        }

        [TestMethod]
        public void TestDiggsLoading()
        {
            var list = parser.GetDiggsList();
            int numberOfDiggsInFile = 55;
            Assert.AreEqual(numberOfDiggsInFile, list.Count);

            var first = list.First();
            Assert.AreEqual("Recenzja polskiej 24hr wojskowej racji żywnościowej [ENG]", first.Title);
            Assert.AreEqual("Smakowało! Fajnie patrzeć jak gość się bawi z panzerwaflami, kulkami mocy itd :)", first.Description);
            Assert.AreEqual(new Uri(@"http://www.wykop.pl/link/2996317/recenzja-polskiej-24hr-wojskowej-racji-zywnosciowej-eng/"), first.Url);
        }

        [TestMethod]
        public void TestDiggDetailsFromFile()
        {
            using (var diggStream = new StreamReader("TestFiles/digg.htm"))
            {
                var diggDetails = parser.LoadDigg(diggStream);
                Assert.AreEqual("@Nensala", diggDetails.AddedBy);

                int numberOfExpectedTags = 4;
                Assert.AreEqual(numberOfExpectedTags, diggDetails.Tags.Count);

                int upVotesExpected = 352;
                Assert.AreEqual(upVotesExpected, diggDetails.UpVotes);

                int downVotesExpected = 15;
                Assert.AreEqual(downVotesExpected, diggDetails.DownVotes);

                string viewsExpected = "12.4 tys.";
                Assert.AreEqual(viewsExpected, diggDetails.Views);

                string descriptionExpected = "Już w przyszłym miesiącu rozpocznie się produkcja pierwszego testowego tunelu będącego elementem systemu transportowego Elona Muska o nazwie Hyperloop, .";
                Assert.AreEqual(descriptionExpected, diggDetails.Description);

                string titleExpected = "Na dniach rozpocznie się budowa prototypowego toru Hyperloop";
                Assert.AreEqual(titleExpected, diggDetails.Title);

                // should be link to digg details
                var expectedUri = new Uri("http://www.wykop.pl/ramka/2825259/na-dniach-rozpocznie-sie-budowa-prototypowego-toru-hyperloop/");
                Assert.AreEqual(expectedUri, diggDetails.Url);

                int expectedNumberOfComments = 59;
                Assert.AreEqual(expectedNumberOfComments, diggDetails.Comments.Count);
                Assert.AreEqual("jaskiniowiec88", diggDetails.Comments.First().User);
                Assert.AreEqual(true, diggDetails.Comments.First().Body.Contains("To jest niesamowity projekt"));

                var tags = new List<string> { "#swiat", "#nauka", "#technologia", "#ciekawostki" };
                for (int i = 0; i < tags.Count; i++)
                {
                    var expectedTag = tags[i];
                    var actualTag = diggDetails.Tags[i];
                    Assert.AreEqual(expectedTag, actualTag);
                }

                var expectedResourceUri = new Uri("http://whatnext.pl/dniach-rozpocznie-sie-budowa-prototypowego-toru-hyperloop/");
                Assert.AreEqual(expectedResourceUri, diggDetails.ResourceUri);
            }
        }

        [TestMethod]
        public void TestDiggRelateds()
        {
            using (var diggStream = new StreamReader("TestFiles/digg.htm"))
            {
                var diggDetails = parser.LoadDigg(diggStream);
                var relatedsList = diggDetails.Relateds;
                int expectedRelateds = 0;
                Assert.AreEqual(expectedRelateds, relatedsList.Count);
            }

            using (var diggStream = new StreamReader("TestFiles/digg_relateds.htm"))
            {
                var diggDetails = parser.LoadDigg(diggStream);
                var relatedsList = diggDetails.Relateds;
                int expectedRelateds = 3;
                Assert.AreEqual(expectedRelateds, relatedsList.Count);

                var relateds = new List<Related> {
                    new Related
                    {
                        Title = "tyle w temacie",
                        Url = new Uri("https://www.youtube.com/watch?v=45656gyQack&t=95")
                    },
                    new Related
                    {
                        Title = "Tutaj trochę lepszy fachowiec od kołkowania",
                        Url = new Uri("https://www.youtube.com/watch?v=UvIyF1Auf_o")
                    },
                    new Related
                    {
                        Title = "Łatabnie dziury w skarpecie po mistrzowsku",
                        Url = new Uri("https://www.youtube.com/watch?v=E0Dl2nROTu8")
                    }
                };

                for (int i = 0; i < relatedsList.Count; i++)
                {
                    var expectedRelated = relateds[i];
                    var actualRelated = relatedsList[i];
                    Assert.AreEqual(expectedRelated.Title, actualRelated.Title);
                    Assert.AreEqual(expectedRelated.Url, actualRelated.Url);
                }
            }
        }
    }
}
