﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Harvester.Core.Utilities;
using System.Collections.Generic;

namespace Harvester.Tests
{
    [TestClass]
    public class EncodingTranslatorTests
    {
        [TestMethod]
        public void TranslationTest()
        {
            var dictionary = new Dictionary<string, string> {
                {@"dziewczyny, bra\u0107 si\u0119", "dziewczyny, brać się"},
                {@"Chocia\u017c jest to wymienione w Podr\u0119czniku U\u017cytkowania wi\u0119kszo\u015b\u0107 ludzi o tym nie wie.",
                    "Chociaż jest to wymienione w Podręczniku Użytkowania większość ludzi o tym nie wie."}
            };

            foreach(var pair in dictionary)
            {
                string encoded = pair.Key;
                string expected = pair.Value;
                string translated = EncodingTranslator.Translate(encoded);
                Assert.AreEqual(expected, translated);
            }
        }
    }
}
