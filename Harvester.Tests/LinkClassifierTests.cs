﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Harvester.Core.Utilities;

namespace Harvester.Tests
{
    [TestClass]
    public class LinkClassifierTests
    {
        private LinkClassifier classifier = new LinkClassifier();

        [TestMethod]
        public void RegularLinkClassification()
        {
            string regularLink = "http://www.wykop.pl/link/2805405/title-here/";
            var regularResult = classifier.Classify(regularLink);
            Assert.AreEqual(LinkClass.Regular, regularResult);
        }

        [TestMethod]
        public void PaidLinkClassification()
        {
            string paidLink = "http://www.wykop.pl/link/partnerredirect/2804637/title-here";
            var paidResult = classifier.Classify(paidLink);
            Assert.AreEqual(LinkClass.Advert, paidResult);
        }

        [TestMethod]
        public void UnknownLinksClassification()
        {
            string[] brokenLinks = new string[] {
                "http://foo.bar/qwerty/foo/bar/",
                "http://foo",
                "http://www.wykop.pl/",
                "http://www.wykop.pl/link/"
            };

            foreach(var brokenLink in brokenLinks)
            {
                var classificationResult = classifier.Classify(brokenLink);
                Assert.AreEqual(LinkClass.Unknown, classificationResult);
            }
        }
    }
}
