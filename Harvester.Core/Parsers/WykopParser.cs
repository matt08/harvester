﻿using Harvester.Core.Interfaces;
using Harvester.Core.Models;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Harvester.Core.Parsers
{
    public class WykopParser : IWykopParser
    {
        private HtmlDocument htmlDocumentMain = new HtmlDocument();

        public WykopParser()
        {
        }

        public WykopParser(TextReader stream)
        {
            htmlDocumentMain = new HtmlDocument();
            htmlDocumentMain.Load(stream);
        }

        public void LoadMainPage()
        {
            LoadPage("http://www.wykop.pl/");
        }

        public void LoadPage(int pageNumber)
        {
            string url = String.Format("http://www.wykop.pl/strona/{0}/", pageNumber);
            LoadPage(url);
        }

        private void LoadPage(string url)
        {
            var htmlWeb = new HtmlWeb();
            htmlDocumentMain = htmlWeb.Load(url);
        }

        public List<Digg> GetDiggsList()
        {
            var diggList = new List<Digg>();

            var nodes = htmlDocumentMain.DocumentNode
                .SelectNodes("//li[contains(@class, 'link iC')]//div[contains(@class, 'lcontrast')]");

            foreach (var diggNode in nodes)
            {
                var titleNode = diggNode.SelectSingleNode("h2//a");
                string title = HtmlEntity.DeEntitize(titleNode.InnerText.Trim());

                string description = diggNode.SelectSingleNode("div[contains(@class, 'description')]//p//a").InnerText;
                description = HtmlEntity.DeEntitize(description.Trim());

                string formattedUri = String.Format("http://www.wykop.pl{0}", titleNode.Attributes["href"].Value);
                Uri url = new Uri(formattedUri);

                Digg digg = new Digg { Title = title, Description = description, Url = url };
                diggList.Add(digg);

            }

            return diggList;
        }

        private string GetDiggDescription(HtmlNode diggNode)
        {
            string description = diggNode.SelectSingleNode("//p[@class='text']//a").InnerText;
            description = HtmlEntity.DeEntitize(description.Trim());

            return description;
        }

        private string GetDiggTitle(HtmlNode diggNode)
        {
            string title = diggNode.SelectSingleNode("//h2//a").InnerText;
            title = HtmlEntity.DeEntitize(title.Trim());

            return title;
        }

        private Uri GetDiggUri(HtmlNode diggNode)
        {
            var titleNode = diggNode.SelectSingleNode("//h2//a[@href]");
            string formattedUri = String.Format("http://www.wykop.pl{0}", titleNode.Attributes["href"].Value);
            Uri url = new Uri(formattedUri);

            return url;
        }

        private string GetAddedBy(HtmlNode detailsNode)
        {
            string addedBy = detailsNode.SelectSingleNode("a[contains(@class, 'affect')]").InnerText;
            addedBy = HtmlEntity.DeEntitize(addedBy.Trim());

            return addedBy;
        }

        private List<string> GetTags(HtmlNode detailsNode)
        {
            var tagNode = detailsNode.SelectNodes("a[@class='tag affect create']");
            List<string> tags = tagNode.Select(x => x.InnerText).ToList();

            return tags;
        }

        private HtmlNode GetDiggNode(HtmlDocument htmlDocument)
        {
            var xPath = "//div[contains(@class, 'article')]//div[contains(@class, 'lcontrast')]";
            var diggNode = htmlDocument.DocumentNode.SelectSingleNode(xPath);

            return diggNode;
        }

        private HtmlNode GetDetailsNode(HtmlNode diggNode)
        {
            var detailsNode = diggNode.SelectSingleNode("//div[@class='fix-tagline']");
            return detailsNode;
        }

        private HtmlNode GetInfoPanelNode(HtmlDocument htmlDocument)
        {
            var infoPanelNode = htmlDocument.DocumentNode.SelectSingleNode("//tr[@class='infopanel']");
            return infoPanelNode;
        }

        private int GetUpVotes(HtmlNode infoPanelNode)
        {
            int upVotes = GetVotes(infoPanelNode, "#voters");
            return upVotes;
        }

        private int GetDownVotes(HtmlNode infoPanelNode)
        {
            int upVotes = GetVotes(infoPanelNode, "#votersBury");
            return upVotes;
        }

        private int GetVotes(HtmlNode infoPanelNode, string hrefValue)
        {
            var xPath = String.Format("td//a[@href='{0}']//b", hrefValue);
            string votesText = infoPanelNode.SelectSingleNode(xPath).InnerText;

            int votes = Int32.Parse(votesText);
            return votes;
        }

        private string GetViewsNumber(HtmlNode infoPanelNode)
        {
            string views = infoPanelNode.SelectSingleNode("td//a[@href='#']//b").InnerText;
            return views;
        }

        private Uri GetResourceUri(HtmlNode diggNode)
        {
            var urlNode = diggNode.SelectSingleNode("//a[@class='affect' and @target='_blank']");
            string url = urlNode.Attributes["href"].Value;

            Uri resourceUrl = new Uri(url);
            return resourceUrl;
        }

        private List<Related> GetRelateds(HtmlDocument htmlDocument)
        {
            var relatedList = htmlDocument.GetElementbyId("relatedList");

            if (relatedList == null)
            {
                return new List<Related>();
            }

            var relatedContent = relatedList.SelectNodes(".//div[@class='content']/a");

            List<Related> relateds = relatedContent.Select(x =>
                {
                    return new Related
                    {
                        Title = x.Attributes["title"].Value,
                        Url = new Uri(x.Attributes["href"].Value)
                    };
                }
                ).ToList();

            return relateds;

        }

        public DiggDetail LoadDigg(TextReader stream)
        {
            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.Load(stream);

            return GetDiggDetail(htmlDocument);
        }

        public DiggDetail LoadDigg(Uri diggUri)
        {
            var htmlWeb = new HtmlWeb();
            HtmlDocument htmlDocumentDetails = htmlWeb.Load(diggUri.AbsoluteUri);

            return GetDiggDetail(htmlDocumentDetails);
        }

        private DiggDetail GetDiggDetail(HtmlDocument htmlDocument)
        {
            var diggNode = GetDiggNode(htmlDocument);
            string title = GetDiggTitle(diggNode);
            string description = GetDiggDescription(diggNode);
            Uri url = GetDiggUri(diggNode);
            Uri resourceUrl = GetResourceUri(diggNode);

            var detailsNode = GetDetailsNode(diggNode);
            string addedBy = GetAddedBy(detailsNode);
            List<string> tags = GetTags(detailsNode);

            var infoPanelNode = GetInfoPanelNode(htmlDocument);
            int upVotes = GetUpVotes(infoPanelNode);
            int downVotes = GetDownVotes(infoPanelNode);
            string views = GetViewsNumber(infoPanelNode);
            List<Comment> comments = GetComments(htmlDocument);

            List<Related> relateds = GetRelateds(htmlDocument);

            var diggDetail = new DiggDetail
            {
                Title = title,
                Url = url,
                Description = description,
                AddedBy = addedBy,
                UpVotes = upVotes,
                DownVotes = downVotes,
                Views = views,
                Tags = tags,
                Comments = comments,
                ResourceUri = resourceUrl,
                Relateds = relateds
            };

            return diggDetail;
        }

        private List<Comment> GetComments(HtmlDocument htmlDocument)
        {
            var rootCommentsNode = htmlDocument.GetElementbyId("itemsStream");

            var commentDiv = ".//div[@data-type='comment']/div";
            var toAuthor = "./div[contains(@class, 'author')]/a[contains(@class, 'showProfileSummary')]";
            var toBody = "./div[@class='text']/p";
            
            var commentDivNodes = rootCommentsNode.SelectNodes(commentDiv);

            var commentList = commentDivNodes.Select(x =>
                {
                    var body = x.SelectSingleNode(toBody);
                    var author = x.SelectSingleNode(toAuthor);
                    return new Comment
                    {
                        User = author.InnerText.Trim(),
                        Body = body.InnerText.Trim()
                    };
                }
                ).ToList();

            return commentList;
        }
    }
}
