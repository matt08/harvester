﻿using Harvester.Core.Interfaces;
using Harvester.Core.Models;
using Harvester.Core.Utilities;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;

namespace Harvester.Core.Parsers
{
    // currently all entries returned with replies
    public class MikroblogParser : IMikroblogParser
    {
        private string lastEntryId;
        private string mainHash;

        public List<MikroblogEntry> GetNewEntries()
        {
            if (String.IsNullOrEmpty(lastEntryId) || String.IsNullOrEmpty(mainHash))
                return LoadMikroblog();

            string baseUrl = "http://www.wykop.pl/ajax2/mikroblog/recent/type/entry/id/{0}/hash/{1}/html/1";
            string formattedUrl = String.Format(baseUrl, lastEntryId, mainHash);

            using(var webClient = new WebClient())
            {
                string jsonContent = webClient.DownloadString(formattedUrl);
                jsonContent = FixHtmlTags(jsonContent);

                string extractHtmlDataPattern = "html\":\"(.+)\"}}]}";
                var matchResult = Regex.Match(jsonContent, extractHtmlDataPattern);
                string properNewHtmlContent = matchResult.Groups[1].Value;
                properNewHtmlContent = FixNewLines(properNewHtmlContent);

                var htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(properNewHtmlContent);

                var newEntries = GetEntriesFromNode(htmlDocument.DocumentNode);
                if(newEntries.Any())
                {
                    lastEntryId = GetLastEntryId(htmlDocument);
                }

                return newEntries;
            }
        }

        private string FixNewLines(string toFix)
        {
            StringBuilder contentFormattedNewLines = new StringBuilder(toFix);
            contentFormattedNewLines.Replace(@"\n", '\n'.ToString());
            contentFormattedNewLines.Replace(@"\r", "");
            contentFormattedNewLines.Replace(@"\t", '\t'.ToString());
            // fix for links
            contentFormattedNewLines.Replace(@"\/", "/");

            return contentFormattedNewLines.ToString();
        }

        private string FixHtmlTags(string toFix)
        {
            string result = toFix;
            result = Regex.Replace(result, @"\\\""", @"""");
            result = Regex.Replace(result, @"\/>", @"/>");
            result = Regex.Replace(result, @"<\\/", @"</");

            return result;
        }

        private string GetLastEntryId(HtmlDocument htmlDocument)
        {
            var itemsStreamNode = htmlDocument.GetElementbyId("itemsStream");
            var lastId = GetEntryDivs(itemsStreamNode).FirstOrDefault().ParentNode.Attributes["data-id"].Value;
            return lastId;
        }

        private IEnumerable<HtmlNode> GetEntryDivs(HtmlNode node)
        {
            if (String.IsNullOrEmpty(node.InnerHtml))
            {
                return new List<HtmlNode>();
            }

            var entryDivs = node.SelectNodes(".//div[contains(@class, 'wblock lcontrast dC')]/div");
            
            return entryDivs;
        }

        private List<MikroblogEntry> GetEntriesFromNode(HtmlNode node)
        {
            var entryDivs = GetEntryDivs(node);
            
            List<MikroblogEntry> entries = entryDivs.Select(div =>
                {
                    MikroblogEntry entry = new MikroblogEntry();
                    entry.UserName = GetEntryUserName(div);
                    entry.Comment = GetEntryComment(div);
                    return entry;
                }
                ).ToList();

            return entries;
        }

        private string GetEntryUserName(HtmlNode div)
        {
            string userName = div.SelectSingleNode(".//a[contains(@class, 'showProfileSummary')]").InnerText;
            return userName;
        }

        private string GetEntryComment(HtmlNode div)
        {
            string comment = div.SelectSingleNode(".//div[@class='text']/p").InnerText;
            comment = Fix(comment);
            return comment;
        }

        private string Fix(string toFix)
        {
            var deEntitized = HtmlEntity.DeEntitize(toFix);
            var fixedEncoding = EncodingTranslator.Translate(deEntitized);
            var commentContent = fixedEncoding.Trim();

            return commentContent;
        }

        public List<MikroblogEntry> LoadMikroblog()
        {
            var htmlWeb = new HtmlWeb();
            htmlWeb.UseCookies = true;
            HtmlDocument htmlDoc = htmlWeb.Load(@"http://www.wykop.pl/mikroblog/wszystkie/");

            lastEntryId = GetLastEntryId(htmlDoc);

            string firstScriptTagContent = htmlDoc.DocumentNode.SelectSingleNode("//script").InnerText;
            string hashPattern = "hash.*:.\"(.+)\",";
            var matchResult = Regex.Match(firstScriptTagContent, hashPattern);
            mainHash = matchResult.Groups[1].Value;

            var currentEntries = GetEntriesFromNode(htmlDoc.GetElementbyId("itemsStream"));

            return currentEntries;
        }

        /// <summary>
        /// Returns number of new entries available.
        /// </summary>
        /// <returns></returns>
        public int CheckNewEntriesAvailable()
        {
            int result = 0;
            if (String.IsNullOrEmpty(lastEntryId) || String.IsNullOrEmpty(mainHash))
                return result;

            string baseUrl = "http://www.wykop.pl/ajax2/mikroblog/recent/type/entry/id/{0}/hash/{1}/";
            string formattedUrl = String.Format(baseUrl, lastEntryId, mainHash);

            using(var client = new HttpClient())
            {
                string jsonContent = client.GetStringAsync(formattedUrl).Result;
                string countPattern = @"count"":([0-9]*)}";
                var matchResult = Regex.Match(jsonContent, countPattern);
                string newEntries = matchResult.Groups[1].Value;
                int.TryParse(newEntries, out result);
            }

            return result;
        }
    }
}
