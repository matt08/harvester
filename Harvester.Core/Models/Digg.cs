﻿using System;

namespace Harvester.Core.Models
{
    public class Digg
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public Uri Url { get; set; }
    }
}
