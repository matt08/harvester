﻿
namespace Harvester.Core.Models
{
    public class MikroblogEntry
    {
        public string UserName { get; set; }
        public string Comment { get; set; }
    }
}
