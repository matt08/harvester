﻿namespace Harvester.Core.Models
{
    public class Comment
    {
        public string User { get; set; }
        public string Body { get; set; }
        public int UpVotes { get; set; }
        public int DownVotes { get; set; }
        // id of parent property
    }
}
