﻿using System;

namespace Harvester.Core.Models
{
    public class Related
    {
        public string Title { get; set; }
        public Uri Url { get; set; }
    }
}
