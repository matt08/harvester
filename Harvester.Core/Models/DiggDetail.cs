﻿using System;
using System.Collections.Generic;

namespace Harvester.Core.Models
{
    public class DiggDetail : Digg
    {
        public string AddedBy { get; set; }
        public string Views { get; set; }
        public int UpVotes { get; set; }
        public int DownVotes { get; set; }
        public Uri ResourceUri { get; set; }
        public List<string> Tags { get; set; }
        public List<Comment> Comments { get; set; }
        public List<Related> Relateds { get; set; }

        public override string ToString()
        {
            var str = String.Format("[**] Title: {0}\n[*] Description: {1}\n[*] Added by: {2}\n[*] Views: {3}\n[*] UpVotes: {4}\n[*] DownVotes: {5}\n[*] Uri: {6}",
                Title, Description, AddedBy, Views, UpVotes, DownVotes, Url.AbsoluteUri
                );

            return str;
        }
    }
}
