﻿using Harvester.Core.Models;
using System;
using System.Collections.Generic;

namespace Harvester.Core.Interfaces
{
    public interface IWykopParser
    {
        DiggDetail LoadDigg(Uri diggUri);
        List<Digg> GetDiggsList();
        void LoadMainPage();
        void LoadPage(int pageNumber);
    }
}
