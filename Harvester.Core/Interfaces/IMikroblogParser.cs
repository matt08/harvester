﻿using Harvester.Core.Models;
using System.Collections.Generic;

namespace Harvester.Core.Interfaces
{
    public interface IMikroblogParser
    {
        List<MikroblogEntry> LoadMikroblog();
        List<MikroblogEntry> GetNewEntries();
        int CheckNewEntriesAvailable();
    }
}
