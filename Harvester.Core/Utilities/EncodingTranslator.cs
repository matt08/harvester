﻿using System.Collections.Generic;
using System.Text;

namespace Harvester.Core.Utilities
{
    public static class EncodingTranslator
    {
        private static Dictionary<string, string> translationDictionary = new Dictionary<string, string>
        {
            {@"\u0104", "Ą"}, {@"\u0106", "Ć"}, {@"\u0118", "Ę"}, {@"\u0141", "Ł"}, {@"\u0143", "Ń"},
            {@"\u00d3", "Ó"}, {@"\u015a", "Ś"}, {@"\u0179", "Ź"}, {@"\u017b", "Ż"},
            {@"\u0105", "ą"}, {@"\u0107", "ć"}, {@"\u0119", "ę"}, {@"\u0142", "ł"}, {@"\u0144", "ń"},
            {@"\u00f3", "ó"}, {@"\u015b", "ś"}, {@"\u017a", "ź"}, {@"\u017c", "ż"}
        };

        public static string Translate(string toTranslate)
        {
            var result = new StringBuilder(toTranslate);

            foreach(var unicodeEntry in translationDictionary)
            {
                result.Replace(unicodeEntry.Key, unicodeEntry.Value);
            }

            return result.ToString();
        }
    }
}
