﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Harvester.Core.Utilities
{
    public enum LinkClass
    {
        Regular,
        Advert,
        Unknown
    }

    public class LinkClassifier
    {
        public LinkClass Classify(string url)
        {
            return Classify(new Uri(url));
        }

        public LinkClass Classify(Uri url)
        {
            try
            {
                string[] segments = url.Segments;

                string segmentToCheck = segments.ElementAtOrDefault(2);

                if (IsRegularLink(segmentToCheck))
                {
                    return LinkClass.Regular;
                }
                else if (IsPaidLink(segmentToCheck))
                {
                    return LinkClass.Advert;
                }
                else
                {
                    return LinkClass.Unknown;
                }
            }
            catch(ArgumentNullException)
            {
                return LinkClass.Unknown;
            }
        }

        private bool IsRegularLink(string linkSegment)
        {
            bool regularLinkMatch = Regex.IsMatch(linkSegment, "[0-9]+");
            return regularLinkMatch;
        }

        private bool IsPaidLink(string linkSegment)
        {
            return linkSegment.Equals("partnerredirect/");
        }
    }
}
