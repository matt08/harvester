﻿using Harvester.Core.Interfaces;
using Harvester.Core.Models;
using System;
using System.Collections.Generic;

namespace Harvester.Gui.Design
{
    public class DesignTimeMikroblog : IMikroblogParser
    {
        private bool load;
        private List<MikroblogEntry> emptyList = new List<MikroblogEntry>();

        public List<MikroblogEntry> LoadMikroblog()
        {
            return new List<MikroblogEntry> {
                new MikroblogEntry {Comment="dfsdfsd", UserName="John Doe"},
                new MikroblogEntry {Comment="sada", UserName="ddfssdlfklsdkf"},
                new MikroblogEntry {Comment="sadweqwieda", UserName="awiewjfiwejfij"},
                new MikroblogEntry {Comment="sadweqsadweqwiedasadweqwiedasadweqwiedasadweqwiedasadweqwiedawieda",
                    UserName="awiewjfiwesadweqwiedasadweqwiedasadweqwiedasadweqwiedasadweqwiedasadweqwiedasadweqwiedajfij"}
            };
        }

        public List<MikroblogEntry> GetNewEntries()
        {
            if(!load)
            {
                load = true;
                return LoadMikroblog();
            }
            return emptyList;
        }

        public int CheckNewEntriesAvailable()
        {
            return 1;
        }
    }

    public class DesignTimeWykop : IWykopParser
    {
        public DiggDetail LoadDigg(Uri diggUri)
        {
            return new DiggDetail {};
        }

        public List<Digg> GetDiggsList()
        {
            return new List<Digg>{
                new Digg{ Description="Digg description.", Title="Digg title.", Url=new Uri("http://www.wykop.pl/")},
                new Digg{ Description="dsadasdkas.", Title="dfsdfsdfds", Url=new Uri("http://www.wykop.pl/")},
            };
        }

        public void LoadMainPage()
        {
            return;
        }

        public void LoadPage(int pageNumber)
        {
            return;
        }
    }

}
