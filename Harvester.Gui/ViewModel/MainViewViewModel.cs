﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Threading;
using Harvester.Core.Interfaces;
using Harvester.Core.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Harvester.Gui.ViewModel
{
    public class MainViewViewModel : ViewModelBase
    {
        public ObservableCollection<Digg> DiggList { get; set; }
        public ObservableCollection<Comment> CurrentComments { get; set; }
        public ObservableCollection<MikroblogEntry> MikroblogComments { get; set; }
        IWykopParser wykopParser;
        IMikroblogParser mikroblogParser;

        #region Commands
        private RelayCommand<Digg> loadCommand;
        public RelayCommand<Digg> LoadCommand
        {
            get
            {
                return loadCommand ?? (loadCommand = new RelayCommand<Digg>(digg => LoadComments(digg)));
            }
        }

        private RelayCommand<string> loadPageCommand;
        public RelayCommand<string> LoadPageCommand
        {
            get
            {
                return  loadPageCommand ?? (loadPageCommand = new RelayCommand<string>(num =>
                    Task.Factory.StartNew(() =>
                    {
                        int pageNumber = int.Parse(num);
                        LoadWykop(pageNumber);
                    })
                    ));
            }
        }
        #endregion

        #region Ctor
        public MainViewViewModel(IMikroblogParser mikroblogParser, IWykopParser wykopParser)
        {
            this.wykopParser = wykopParser;
            this.mikroblogParser = mikroblogParser;

            DiggList = new ObservableCollection<Digg>();
            CurrentComments = new ObservableCollection<Comment>();
            MikroblogComments = new ObservableCollection<MikroblogEntry>();
            
            Task.Factory.StartNew(() => LoadWykop());
            Task.Factory.StartNew(LoadMikroblog);
        }
        #endregion

        private void LoadMikroblog()
        {
            while (true)
            {
                var entries = mikroblogParser.GetNewEntries();
                for (int i = entries.Count-1; i >= 0; i--)
                {
                    var item = entries[i];
                    DispatcherHelper.CheckBeginInvokeOnUI(() => MikroblogComments.Insert(0, item));
                }
                Task.Delay(15000).Wait();
            }
        }

        private void LoadComments(Digg digg)
        {
            var diggDetails = wykopParser.LoadDigg(digg.Url);
            DispatcherHelper.CheckBeginInvokeOnUI(() => CurrentComments.Clear());
            foreach (var comment in diggDetails.Comments)
            {
                DispatcherHelper.CheckBeginInvokeOnUI(() => CurrentComments.Add(comment));
            }
        }

        private void LoadWykop(int page=1)
        {
            wykopParser.LoadPage(page);
            var list = wykopParser.GetDiggsList();
            DispatcherHelper.CheckBeginInvokeOnUI(() => DiggList.Clear());
            foreach (var dig in list)
            {
                DispatcherHelper.CheckBeginInvokeOnUI(() => DiggList.Add(dig));
            }
        }
    }
}
