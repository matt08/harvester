﻿using GalaSoft.MvvmLight;
using Harvester.Core.Interfaces;
using Harvester.Core.Parsers;
using Harvester.Gui.Design;
using StructureMap;

namespace Harvester.Gui.ViewModel
{
    public class ViewModelLocator
    {
        public MainViewViewModel Main
        {
            get
            {
                return container.GetInstance<MainViewViewModel>();
            }
        }

        private static Container container;

        static ViewModelLocator()
        {
            container = new Container(c =>
            {
                if (ViewModelBase.IsInDesignModeStatic)
                {
                    GalaSoft.MvvmLight.Threading.DispatcherHelper.Initialize();
                    c.For<IMikroblogParser>().Use<DesignTimeMikroblog>();
                    c.For<IWykopParser>().Use<DesignTimeWykop>();
                }
                else
                {
                    c.For<IMikroblogParser>().Use<MikroblogParser>();
                    c.For<IWykopParser>().Use<WykopParser>(() => new WykopParser());
                }
            });
        }
    }
}
