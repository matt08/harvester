﻿using Harvester.Core;
using System.Windows;
using System.Windows.Controls;

namespace Harvester.Gui.View
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : Window
    {
        public MainView()
        {
            InitializeComponent();
        }
    }
}
